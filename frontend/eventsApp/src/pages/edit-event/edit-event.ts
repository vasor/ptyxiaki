import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { EventsService } from '../../providers/events-service';
import { HomePage } from '../home/home';
import { GoogleMapsAddPage } from '../google-maps-add/google-maps-add';

@Component({
  selector: 'page-edit-event',
  templateUrl: 'edit-event.html'
})
export class EditEventPage {
  public event: any;
  public eventform: FormGroup;
  public title: any;
  public description: any;
  public price: any;
  public date: String;
  public starting_time: any;
  public expected_duration: any;
  public toedit: Boolean;
  public pastEvent: Boolean;

  constructor(
    private alertCtrl: AlertController, 
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private formBuilder: FormBuilder, 
    private eventsService: EventsService
  ) {
    this.event = this.navParams.data;
    
    this.eventform = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: [''],
      date: ['', Validators.required],
      starting_time: [
        '', Validators.compose([Validators.required, Validators.pattern('^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')])
      ],
      expected_duration: ['', Validators.compose([Validators.required, Validators.pattern('^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')])]
    });
    this.title = this.event.title;
    this.description = this.event.description;
    this.price = this.event.price;
    this.starting_time = this.event.starting_time;
    this.expected_duration = this.event.expected_duration;
    this.toedit = true;
    this.pastEvent = false;
  }
  ionViewDidLoad(){
    let today = new Date();
    let todayString = this.formatDate(today);
    let stringDate = this.formatStringDate(this.date);
    if (todayString > stringDate) {
      this.pastEvent = true;
    }
  }
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
  }
  formatStringDate(date) {
    var dateList = date.split("/");
    return [dateList[2], dateList[1], dateList[0]].join("/");
  }
  deleteEvent() {
    let confirm = this.alertCtrl.create({
      title: 'Delete Event',
      message: 'Are you sure you want to delete this event?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed',
          handler: () => {
            console.log('Proceed clicked');
            this.eventsService.deleteEvent(this.event._id).subscribe(data => {
              console.log(data);
              this.navCtrl.setRoot(HomePage)
            }, error => {
              console.log(error)
              let alert = this.alertCtrl.create({
                title: 'Oops!',
                subTitle: 'Something went wrong. Please try again!',
                buttons: ['OK']
              });
              alert.present();
            });
          }
        }
      ]
    });
    confirm.present();

  }

  sendForm() {
    if (this.price == undefined || this.price == ' ' || this.price == '') {
      this.price = 'free';
    }
    let confirm = this.alertCtrl.create({
      title: 'Proceed?',
      message: 'Title: ' + this.title + '<br>' + 'Description: ' + this.description + '<br>' + 'Price: ' + this.price + '<br>' + 'Date: ' + this.date + '<br>' + 'Starting Time: ' + this.starting_time + '<br>' + 'Expected Duration: ' + this.expected_duration,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed',
          handler: () => {
            console.log('Proceed clicked');
            this.navCtrl.push(GoogleMapsAddPage, {
              event_id: this.event._id,
              address: this.event.address,
              coordinates: this.event.coordinates,
              edit: this.toedit,
              title: this.title,
              description: this.description,
              price: this.price,
              date: this.date,
              starting_time: this.starting_time,
              expected_duration: this.expected_duration,
            });
          }
        }
      ]
    });
    confirm.present();
  }

}
