import { Component } from '@angular/core';
import { EventsService } from '../../providers/events-service';
import { MapsService } from '../../providers/maps-service';
import { AlertController, NavController, Platform, LoadingController } from 'ionic-angular';
import { User } from '../../providers/user';
import { AuthService } from '../../providers/auth-service'
import { Network } from '@ionic-native/network';
import { EventModal } from '../event-modal/event-modal';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';


@Component({
    selector: 'page-today',
    templateUrl: 'today.html'
})
export class TodayPage {


    list: any;
    public current_position: any;
    public default_lat: number = 37.975677;
    public default_lng: number = 23.734140;
    public loader: any;
    public showMessage: Boolean;
    public noEvents: Boolean;
    public disconnected: Boolean;
    public imgTag1: String;
    public imgTag2: string;
    private watch: any;
    private loader_present: Boolean;

    constructor(public menuCtrl: MenuController, public modalCtrl: ModalController, public sanitizer: DomSanitizer, public alertCtrl: AlertController, public network: Network, public navCtrl: NavController, public userService: User, public auth: AuthService, public eventService: EventsService, public mapService: MapsService, public platform: Platform, public loadingCtrl: LoadingController) {
        this.eventService = eventService;
        this.mapService = mapService;
        this.navCtrl = navCtrl;
        this.userService = userService;
        this.auth = auth;
        this.noEvents = true;
        this.disconnected = false;
        this.sanitizer = sanitizer;
        this.loader_present = false;
    }

    ionViewDidLoad() {
        this.platform.ready().then(() => {
            this.loader = this.loadingCtrl.create({
                content: "Please wait..."
            });

            this.loader.present();
            this.loader_present = true;
            setTimeout(() => {
                if (this.loader_present) {
                    this.loader.dismiss();
                    let alert = this.alertCtrl.create({
                        title: 'Loading Took Too Long',
                        subTitle: 'Please try refreshing the page',
                        buttons: ['OK']
                    })
                    alert.present();
                }
            }, 20000);
            this.initList();
            if (this.auth.loggedIn()) {
                console.log("logged in", this.auth.user);
            }
            else {
                console.log("not logged in")
            }

            this.network.onDisconnect().subscribe(() => {
                this.disconnected = true;
                let alert = this.alertCtrl.create({
                    title: 'Internet Disconnected',
                    subTitle: 'Please connect and refresh page',
                    buttons: ['OK']
                })
                alert.present();
            })
        });
    }


    initList() {
        this.eventService.getListAll().subscribe(
            (data) => {
                console.log(data)
                this.list = data;
                if (this.list.length != 0) {
                    this.noEvents = false;
                    this.loader.dismiss();
                    this.loader_present = false;
                }
            }
        )
    }

    doRefresh() {
        this.navCtrl.setRoot(this.navCtrl.getActive().component)
    }

    openModal(event) {
        let eventModal = this.modalCtrl.create(EventModal, { 'event': event });
        eventModal.onDidDismiss((data) => {
            this.initList();
        });
        eventModal.present();
    }
}

