import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import { SuccessPage } from '../success/success';

@Component({
  selector: 'page-google-maps-add',
  templateUrl: 'google-maps-add.html'
})
export class GoogleMapsAddPage {

  @ViewChild('map') mapElement;
  @ViewChild('pacinput') pacElement;
  public map: any;
  pacinput: any;
  public coordinates: any;
  public searchInput: any;
  public title: String;
  public description: String;
  public address: String;
  public price: any;
  public date: any;
  public starting_time: any;
  public expected_duration: any;
  public pac_input: any;
  public locations: any;
  public toedit: Boolean;
  public default_lat: number = 37.975342;
  public default_lng: number = 23.735084;
  public event_id: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public alertCtrl: AlertController) {
    this.navParams = navParams;
    this.navCtrl = navCtrl;
    this.title = navParams.data.title;
    this.description = navParams.data.description;
    this.price = navParams.data.price;
    this.date = navParams.data.date;
    this.starting_time = navParams.data.starting_time;
    this.expected_duration = navParams.data.expected_duration;
    this.coordinates = navParams.data.coordinates;
    this.toedit = false;
    this.toedit = navParams.data.edit;
    this.event_id = navParams.data.event_id
    if (this.toedit == true) {
      this.default_lat = this.coordinates.lat;
      this.default_lng = this.coordinates.lng;
      this.locations = [{ 'address': navParams.data.address, 'position': navParams.data.coordinates }];

      this.searchInput = navParams.data.address;

    }
    console.log("coordinates", this.coordinates, "address", this.address, "toedit", this.toedit, "locations", this.locations, "event_id", this.event_id)
  }

  ionViewDidLoad() {
    console.log(this.navParams.data)
    this.platform.ready().then(() => {
      this.initMap();
    });
  }


  initMap() {
    let latLng = new google.maps.LatLng(this.default_lat, this.default_lng);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // Create the search box and link it to the UI element.
    let input = this.pacElement.nativeElement;
    let searchBox = new google.maps.places.SearchBox(input);
    this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // Bias the SearchBox results towards current map's viewport.
    this.map.addListener(this.map, 'bounds_changed', function () {
      searchBox.setBounds(this.map.getBounds());
    });
    //  if it comes from edit, show marker in elected address
    if (this.toedit == true) {
      let defaultmarker = new google.maps.Marker({
        position: latLng,
        map: this.map
      });
    }
    let markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.

    searchBox.addListener('places_changed', () => {
      let places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];
      let counter = 0;
      let map = this.map
      // For each place, get the icon, name and location.
      let bounds = new google.maps.LatLngBounds();


      places.forEach(function (place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        let icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));
        markers[counter].address = place.formatted_address;
        console.log("place: ", place);
        counter += 1;
        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      this.locations = markers;
      this.map.fitBounds(bounds);
    });
  }

  selectAddress() {
    if (this.locations == undefined) {
      let alert = this.alertCtrl.create({
        title: 'Something went wrong',
        subTitle: 'You did not select any addresses',
        buttons: ['Try Again']
      });
      alert.present();
    }
    else {
      console.log(this.locations);
      let alert = this.alertCtrl.create({
        title: 'Is this the right address?',
        message: this.locations[0].address,
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes, proceed!',
            handler: () => {
              console.log('Buy clicked');
              console.log(this.toedit)
              this.navCtrl.push(SuccessPage, {
                edit: this.toedit,
                event_id: this.event_id,
                title: this.title,
                description: this.description,
                address: this.locations[0].address,
                price: this.price,
                date: this.date,
                starting_time: this.starting_time,
                expected_duration: this.expected_duration,
                position: this.locations[0].position,
              });
            }
          }
        ]
      });
      alert.present();
    }
  }

  centerMapMyLocation() {
    let watch = Geolocation.getCurrentPosition()
    watch.then((data) => {

      this.default_lat = data.coords.latitude;
      this.default_lng = data.coords.longitude;
      this.centerMap({ 'lat': this.default_lat, 'lng': this.default_lng });
      let latLng = new google.maps.LatLng(this.default_lat, this.default_lng);
      let marker = new google.maps.Marker({
        position: latLng,
        map: this.map
      });

      let geocoder = new google.maps.Geocoder;
      geocoder.geocode({ 'location': latLng }, (results, status) => {
        if (status.toString() == 'OK') {
          if (results[0]) {
            this.map.setZoom(15);
            var marker = new google.maps.Marker({
              position: latLng,
              map: this.map
            });
            this.locations = [{ 'position': latLng, 'address': results[0].formatted_address }]
            this.openInfo(results[0].formatted_address, this.default_lat, this.default_lng);


          } else {
            console.log('No results found');
          }
        } else {
          console.log('Geocoder failed due to: ' + status);
        }
      });


    }, (err) => {
      console.log(err)
    })

  }

  openInfo(address, lat, lng) {
    let contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h3 id="firstHeading" class="firstHeading">' + address + '</h3>' +
      '</div>';
    let infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    let latLng = new google.maps.LatLng(lat, lng);
    let marker = new google.maps.Marker({
      position: latLng,
      map: this.map
    });
    infowindow.open(this.map, marker);

  }

  centerMap(coords) {
    console.log(coords)

    let latLng = new google.maps.LatLng(coords.lat, coords.lng);
    this.map.setCenter(latLng);
  }


}

