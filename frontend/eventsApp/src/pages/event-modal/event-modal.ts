import { Component, ViewChild } from '@angular/core';
import { EventsService } from '../../providers/events-service';
import { MapsService } from '../../providers/maps-service';
import { NavController, Platform, NavParams, ViewController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'modal-event',
    templateUrl: 'event-modal.html'
}

)
export class EventModal {
    public event: any;
    @ViewChild('detMap') mapElement;
    public map: any;
    public openedMap: Boolean;
    public default_lat: any;
    public default_lng: any;
    public current_position: any;

    constructor(public platform: Platform, public mapService: MapsService, public eventService: EventsService, public nav: NavController, public sanitizer: DomSanitizer, public navParams: NavParams, public viewCtrl: ViewController) {
        this.event = []
        this.nav = nav;
        this.platform = platform;
        this.mapService = mapService;
        this.event = this.navParams.get('event');
        this.event['img'] = this.sanitizer.bypassSecurityTrustUrl(this.event['img']);
        this.openedMap = false;
    }

    initMap() {
        let watch = this.mapService.getCurrentPosition();
        watch.then((data) => {
            this.current_position = data.coords;
            this.default_lat = this.current_position.latitude;
            this.default_lng = this.current_position.longitude;
            console.log(this.current_position)
            let latLng = new google.maps.LatLng(this.event.coordinates.lat, this.event.coordinates.lng);
            let mylatLng = new google.maps.LatLng(this.default_lat, this.default_lng);
            let mapOptions = {
                center: latLng,
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
            let marker = new google.maps.Marker({
                position: latLng,
                map: this.map
            });
            let mymarker = new google.maps.Marker({
                position: mylatLng,
                map: this.map
            });


        }
        );
    }
    dismiss() {
        this.viewCtrl.dismiss();
    }

    openMap() {
        this.openedMap = true;
        this.initMap();
    }
    showDetails() {
        this.openedMap = false;
    }
    centerMapMyLocation() {
        if (this.default_lat && this.default_lng) {
            let latLng = new google.maps.LatLng(this.default_lat, this.default_lng);
            this.map.setCenter(latLng);
        }
    }

}
