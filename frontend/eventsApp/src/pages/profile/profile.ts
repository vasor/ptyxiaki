import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { User } from '../../providers/user'
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AddPage } from '../add/add';
import { EditEventPage } from '../edit-event/edit-event';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  private user_id: any;
  private username: any;
  private firstname: any;
  private lastname: any;
  private email: any;
  private events: any;
  private registered: any;
  
  constructor(public sanitizer: DomSanitizer, public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public userService: User) {
    this.navCtrl = navCtrl;
    this.auth = auth;
    this.userService = userService;
    this.sanitizer = sanitizer;

  }

  ionViewDidLoad() {

    this.user_id = this.auth.user;
    this.userService.getInfo(this.user_id).subscribe((data) => {
      console.log(data)
      this.username = data['username'];
      this.firstname = data['name'];
      this.lastname = data['surname'];
      this.email = data['email'];
      this.events = data['events']
      this.registered = data['registered_at']
      let today = new Date().toJSON().split('T')[0];
      let todays = today.split('-');
      today = todays[2] + '/' + todays[1] + '/' + todays[0];
   
      for (var i = 0; i < this.events.length; i++) {
        if (this.events[i].date < today) {
          this.events[i].isPast = true;

        }
        if (this.events[i].img == 'null' || this.events[i].img == undefined || this.events[i].img == null || this.events[i].img == '') {
          this.events[i].img = 'assets/event.jpg';
        }
        else {
          this.events[i].img = this.sanitizer.bypassSecurityTrustUrl(this.events[i].img);
        }
      }
      this.events = this.events.sort((eventA, eventB) => {
        const listA = eventA.date.split("/");
        const evA = [listA[2], listA[1], listA[0]].join("/");
        const listB = eventB.date.split("/");
        const evB = [listB[2], listB[1], listB[0]].join("/");
        if (evA > evB) {
          return -1;
        } else if (evA < evB) {
          return 1;
        } else {
          return 0;
        }
      });
    },
      error => {
        console.log(error)
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: 'Something went wrong',
          buttons: ['OK']
        });
        alert.present();
        this.navCtrl.setRoot(HomePage);
      });
  }

  editEvent(event) {
    let today = new Date();
    let todayString = this.formatDate(today);
    let stringDate = this.formatStringDate(event.date);
    if (todayString > stringDate) {
      let alert = this.alertCtrl.create({
        title: 'Cannot edit a past event!',
        subTitle: 'Title: ' + event.title + '<br>' + 'Description: ' + event.description + '<br>' + 'Price: ' + event.price + '<br>' + 'Date: ' + event.date + '<br>' + 'Starting Time: ' + event.starting_time + '<br>' + 'Expected Duration: ' + event.expected_duration,
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.navCtrl.push(EditEventPage, event);
    }
  }
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('/');
  }
  formatStringDate(date) {
    var dateList = date.split("/");
    return [dateList[2], dateList[1], dateList[0]].join("/");
  }
  addEvent() {
    this.navCtrl.push(AddPage);
  }
}
