import { Component, ViewChild, ElementRef } from '@angular/core';
import { EventsService } from '../../providers/events-service';
import { MapsService } from '../../providers/maps-service';
import { AlertController, NavController, Platform, LoadingController } from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import { User } from '../../providers/user';
import { AuthService } from '../../providers/auth-service'
import { Network } from '@ionic-native/network';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Diagnostic } from '@ionic-native/diagnostic';
import { AndroidPermissions } from '@ionic-native/android-permissions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement;
  public map: any;
  list: any;
  public current_position: any;
  public default_lat: number = 37.975677;
  public default_lng: number = 23.734140;
  public loader: any;
  public message: String;
  public message2: String;
  public noEvents: Boolean;
  public disconnected: Boolean;
  private loader_present: Boolean;
  constructor(private androidPermissions: AndroidPermissions, private diagnostic: Diagnostic, public sanitizer: DomSanitizer, public alertCtrl: AlertController, public network: Network, public navCtrl: NavController, public userService: User, public auth: AuthService, public eventService: EventsService, public mapService: MapsService, public platform: Platform, public loadingCtrl: LoadingController) {
    this.eventService = eventService;
    this.mapService = mapService;
    this.navCtrl = navCtrl;
    this.userService = userService;
    this.auth = auth;
    this.noEvents = true;
    this.disconnected = false;
    this.sanitizer = sanitizer;
    this.message = '';
    this.message2 = '';
    this.loader_present = false;
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then(
        success => {
          this.message2 = 'access fine location permission granted';
          console.log('Permission granted')
          this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
            success => {
              this.message2 = 'access COARSE location permission granted';
              console.log('Permission granted')
              this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_LOCATION_EXTRA_COMMANDS).then(
                success => {
                  this.message2 = 'access fine location permission granted';
                  console.log('Permission granted')
                  this.start();
                },
                err => this.androidPermissions.requestPermissions(this.androidPermissions.PERMISSION.ACCESS_LOCATION_EXTRA_COMMANDS)
                  .then((req) => {
                    this.message = req;
                  })
                  .catch((err) => {
                    this.message = err;
                    let alert = this.alertCtrl.create({
                      title: 'Access Denied',
                      subTitle: 'Please give the right permissions',
                      buttons: ['OK']
                    })
                    alert.present();
                  })
              );
            },
            err => this.androidPermissions.requestPermissions(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
              .then((req) => {
                this.message = req;
              })
              .catch((err) => {
                this.message = err;
                let alert = this.alertCtrl.create({
                  title: 'Access Denied',
                  subTitle: 'Please give the right permissions',
                  buttons: ['OK']
                })
                alert.present();
              })
          );

        },
        err => this.androidPermissions.requestPermissions(this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
          .then((req) => {
            this.message = req;
          })
          .catch((err) => {
            this.message = err;
            let alert = this.alertCtrl.create({
              title: 'Access Denied',
              subTitle: 'Please give the right permissions',
              buttons: ['OK']
            })
            alert.present();
          })
      );
    });
  }
  start() {
    let successCallback = (isAvailable) => {
      this.message = 'Is available? ' + isAvailable;

      console.log('Is available? ' + isAvailable);
      if (!isAvailable) {
        let alert = this.alertCtrl.create({
          title: 'GPS Disabled',
          subTitle: 'Please enable GPS and refresh page',
          buttons: ['OK']
        })
        alert.present();
        this.diagnostic.switchToLocationSettings()
      }
      else {
        this.diagnostic.isLocationAuthorized().then(isit => {
          if (isit) {
            this.message = "locationauthorized";
            this.doTheInit();
          }
          else {
            this.ionViewDidLoad()
          }
        }).catch(err => {
          this.message = err.message
        })
      }
    }

    let errorCallback = (e) => {
      console.error(e)
      this.message = e;
      let alert = this.alertCtrl.create({
        title: 'GPS Disabled',
        subTitle: 'Please enable GPS and refresh page',
        buttons: ['OK']
      })
      alert.present();
      this.diagnostic.switchToLocationSettings()

    };

    this.diagnostic.isLocationEnabled().then(successCallback).catch(errorCallback);
  }


  doTheInit() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait..."
    });

    this.loader.present();
    this.loader_present = true;
    setTimeout(() => {
      if (this.loader_present) {
        this.loader.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Loading Took Too Long',
          subTitle: 'Please try refreshing the page',
          buttons: ['OK']
        })
        alert.present();
      }
    }, 20000);
    this.initList();

    if (this.auth.loggedIn()) {
      console.log("logged in", this.auth.user);
    }
    else {
      console.log("not logged in")
    }

    this.network.onDisconnect().subscribe(() => {
      this.disconnected = true;
      let alert = this.alertCtrl.create({
        title: 'Internet Disconnected',
        subTitle: 'Please connect and refresh page',
        buttons: ['OK']
      })
      alert.present();
    })
  }

  initMap() {
    this.message2 = "in initmap"
    let watch = this.mapService.getCurrentPosition();

    watch.then((data) => {
      this.current_position = data.coords;
      this.default_lat = this.current_position.latitude;
      this.default_lng = this.current_position.longitude;
      this.message2 = "inside init Map";

      let latLng = new google.maps.LatLng(this.default_lat, this.default_lng);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let marker = new google.maps.Marker({
        position: latLng,
        map: this.map
      });
      let numberOfEvents = this.list.length;


      for (var i = 0; i < numberOfEvents; i++) {
        if (this.list[i].img == 'null' || this.list[i].img == undefined || this.list[i].img == null || this.list[i].img == '') {
          this.list[i].img = 'assets/event.jpg';
        }
        else if (this.list[i].img != 'assets/event.jpg') {
          this.list[i].img = this.sanitizer.bypassSecurityTrustUrl(this.list[i].img);

        }

        let contentString = '<div id="content">' +
          '<div id="siteNotice">' +
          '</div>' +
          '<h3 id="firstHeading" class="firstHeading">' + this.list[i].title + '</h3>' +
          '<div id="bodyContent">' +
          '<p>' + this.list[i].description + '</p>' +
          '<p>' + this.list[i].starting_time + '</p>' +
          '<p>' + this.list[i].address + '</p>' +
          '<p> Expected Duration: ' + this.list[i].expected_duration + '</p>' +
          '<p>' + this.list[i].price + '</p>' +
          '<p> Created By: ' + this.list[i].created_by + '</p>' +
          '</div>' +
          '</div>';
        let infowindow = new google.maps.InfoWindow({
          content: contentString
        });
        let latLng = new google.maps.LatLng(this.list[i].coordinates.lat, this.list[i].coordinates.lng);
        let marker = new google.maps.Marker({
          position: latLng,
          map: this.map
        });
        marker.addListener('click', function () {
          infowindow.open(this.map, marker);
        });

      }
      this.loader.dismiss();
      this.loader_present = false;

    }).catch((err) => {
      this.message = err.message;
    });
  }

  initList() {
    this.eventService.getList().subscribe(
      (data) => {
        this.applyHaversine(data).then((data2) => {
          this.list = data2;
          if (this.list.length != 0) {
            this.noEvents = false;

          }
          this.message = "before this.initmap"
          this.initMap();

        }, (error) => {
          console.log("errorhavershine")
          if (this.list.length != 0) {
            this.noEvents = false;
          }
        });

      }
    )
  }

  centerMap(coords) {
    console.log(coords)

    let latLng = new google.maps.LatLng(coords.lat, coords.lng);
    this.map.setCenter(latLng);
  }

  centerOpen(event) {
    this.centerMap(event.coordinates);
    this.openInfo(event);
  }
  centerMapMyLocation() {
    if (this.default_lat && this.default_lng) {
      this.centerMap({ 'lat': this.default_lat, 'lng': this.default_lng });
    }
  }

  openInfo(event) {
    let contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h3 id="firstHeading" class="firstHeading">' + event.title + '</h3>' +
      '<div id="bodyContent">' +
      '<p>' + event.description + '</p>' +
      '<p>' + event.starting_time + '</p>' +
      '<p>' + event.address + '</p>' +
      '<p> Expected Duration: ' + event.expected_duration + '</p>' +
      '<p>' + event.price + '</p>' +
      '<p> Created By: ' + event.created_by + '</p>' +
      '</div>' +
      '</div>';
    let infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    let latLng = new google.maps.LatLng(event.coordinates.lat, event.coordinates.lng);
    let marker = new google.maps.Marker({
      position: latLng,
      map: this.map
    });
    infowindow.open(this.map, marker);

  }


  applyHaversine(locations) {
    let watch = Geolocation.getCurrentPosition();
    watch.then((data) => {
      this.current_position = data.coords;
      this.default_lat = this.current_position.latitude;
      this.default_lng = this.current_position.longitude;
      let usersLocation = {
        lat: this.current_position.latitude,
        lng: this.current_position.longitude
      };
      locations.map((location) => {
        let placeLocation = {
          lat: location.coordinates.lat,
          lng: location.coordinates.lng
        };
        location.distance = this.getDistanceBetweenPoints(
          usersLocation,
          placeLocation,
          'km'
        ).toFixed(2);
      });

      //sort the results on distance
      locations = locations.sort((locationA, locationB) => {
        // console.log("inside sort", locationA, locationB)
        if (locationA.distance < locationB.distance) {
          return -1;
        } else if (locationA.distance > locationB.distance) {
          return 1;
        } else {
          return 0;
        }
      });

      return locations;
    });
    return new Promise((resolve, reject) => {
      resolve(locations)
    })
  }
  getDistanceBetweenPoints(start, end, units) {

    let earthRadius = {
      miles: 3958.8,
      km: 6371
    };

    let R = earthRadius[units || 'miles'];
    let lat1 = start.lat;
    let lon1 = start.lng;
    let lat2 = end.lat;
    let lon2 = end.lng;

    let dLat = this.toRad((lat2 - lat1));
    let dLon = this.toRad((lon2 - lon1));
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.toRad(lat1)) * Math.cos(this.toRad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;

    return d;

  }

  toRad(x) {
    return x * Math.PI / 180;
  }


  doRefresh() {
    this.ionViewDidLoad()
  }
}
