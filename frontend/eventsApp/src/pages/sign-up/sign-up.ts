import { Component, ViewChild, NgModule } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../providers/user';
import { HomePage } from '../home/home';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})

export class SignUpPage {

  public slideOneForm: FormGroup;

  submitAttempt: boolean = false;
  public username: String;
  public email: String;
  public firstname: String;
  public lastname: String;
  public password: String;
  public repassword: String;
  public passwordGroup: any;
  public not_matching: Boolean;
  public loading: any;
  public result: any;
  public token: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public userService: User, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.slideOneForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*')])],
      firstName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      lastName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.pattern(/^[a-z0-9!#$%&' * +\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)])],
      password: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
      repassword: ['', Validators.compose([Validators.maxLength(30), Validators.required])]
    }, {
        validator: this.matchingPasswords('password', 'repassword')
      });
    this.passwordGroup = this.slideOneForm.controls['matching_password'];
    this.navCtrl = navCtrl;
    this.result = []
  }

  matchPassword(group): any {
    let password = group.controls.password;
    let confirm = group.controls.confirm;

    // Don't kick in until user touches both fields
    if (password.pristine || confirm.pristine) {
      return null;
    }

    // Mark group as touched so we can add invalid class easily
    group.markAsTouched();
    if (password.value === confirm.value) {
      return null;
    }

    return {
      isValid: false
    };
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        this.not_matching = true;
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }

  save() {
    this.submitAttempt = true;
    if (this.slideOneForm.valid) {
      this.loading = this.loadingCtrl.create({ content: 'Loading' });
      console.log("success!", this.username, this.lastname, this.firstname, this.email, this.password)
      this.userService.signup(this.firstname, this.lastname, this.username, this.email, this.password)
        .subscribe((data) => {
          console.log(data['token'])
          this.token = data['token']
          this.saveJwt(this.token)
          this.navCtrl.setRoot(HomePage)
        },
        error => {
          console.log(error)
          let alert = this.alertCtrl.create({
            title: 'Oops!',
            subTitle: error.json()['message'],
            buttons: ['OK']
          });
          alert.present();

        },
        () => this.loading.dismiss);
    }
    else {
      console.log("not valid")
    }
  }




  saveJwt(jwt) {
    console.log(jwt)
    if (jwt) {
      localStorage.setItem('token', jwt)
    }
  }


}
