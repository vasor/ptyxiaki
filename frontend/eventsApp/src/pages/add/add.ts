import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { GoogleMapsAddPage } from '../../pages/google-maps-add/google-maps-add'

/*
  Generated class for the Add page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add',
  templateUrl: 'add.html'
})
export class AddPage {
  private eventform: FormGroup;
  public hours: any;
  public minute: any;
  public title: String;
  public description: String;
  public price: any;
  public date: any;
  public starting_time: any;
  public expected_duration: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public alertCtrl: AlertController) {
    this.eventform = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      price: [''],
      date: ['', Validators.required],
      starting_time: ['', Validators.compose([Validators.required, Validators.pattern('^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')])],
      expected_duration: ['', Validators.compose([Validators.required, Validators.pattern('^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')])]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPage');
  }

  sendForm() {
    if (this.price == undefined || this.price == ' ' || this.price == '') {
      this.price = 'free';
    }
    let confirm = this.alertCtrl.create({
      title: 'Proceed?',
      message: 'Title: ' + this.title + '<br>' + 'Description: ' + this.description + '<br>' + 'Price: ' + this.price + '<br>' + 'Date: ' + this.date + '<br>' + 'Starting Time: ' + this.starting_time + '<br>' + 'Expected Duration: ' + this.expected_duration,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Proceed',
          handler: () => {
            console.log('Proceed clicked');
            this.navCtrl.push(GoogleMapsAddPage, {
              title: this.title, description: this.description, price: this.price, date: this.date, starting_time: this.starting_time, expected_duration: this.expected_duration,
            });
          }
        }
      ]
    });
    confirm.present();
  }

}
