import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AddPage } from '../add/add'

/*
  Generated class for the AddButton page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-button',
  templateUrl: 'add-button.html'
})
export class AddButtonPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddButtonPage');
  }

  goToAdd() {
    this.navCtrl.push(AddPage)
  }

}
