import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EventsService } from '../../providers/events-service';
import { HomePage } from '../home/home'

@Component({
  selector: 'page-success',
  templateUrl: 'success.html'
})
export class SuccessPage {
  public title: String;
  public description: String;
  public address: String;
  public price: any;
  public date: any;
  public starting_time: any;
  public expected_duration: any;
  public position: any;
  public toedit: Boolean;
  public event_id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public eventsService: EventsService) {
    this.navParams = navParams;
    this.navCtrl = navCtrl;
    this.eventsService = eventsService;
    this.title = navParams.data.title;
    this.description = navParams.data.description;
    this.address = navParams.data.address;
    this.price = navParams.data.price;
    this.date = navParams.data.date;
    this.starting_time = navParams.data.starting_time;
    this.expected_duration = navParams.data.expected_duration;
    this.position = navParams.data.position;
    this.event_id = navParams.data.event_id;
    this.toedit = navParams.data.edit;
  }

  saveEvent() {
    if (this.toedit) {
      this.eventsService.editEvent(this.event_id, this.title, this.description, this.address, this.price, this.date, this.starting_time, this.expected_duration, this.position).then(
        (data) => {
          this.navCtrl.setRoot(HomePage);

        },
        (error) => {
          console.log(error)
        }
      );
      this.navCtrl.setRoot(HomePage);
    }
    else {
      this.eventsService.addEvent(this.title, this.description, this.address, this.price, this.date, this.starting_time, this.expected_duration, this.position).then(
        (data) => {
          this.navCtrl.setRoot(HomePage);

        },
        (error) => {
          console.log(error)
        }
      );
    }

  }

}
