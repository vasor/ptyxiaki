import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { SignUpPage } from '../sign-up/sign-up';
import { Component, ViewChild } from '@angular/core';
import { HomePage } from '../home/home';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'login.html',
})
export class LoginPage {
  authType: string = "login";
  public loginForm: FormGroup;
  private username: any;
  private password: any;
  constructor(public events: Events, private auth: AuthService, public nav: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z]*')])],
      password: ['', Validators.compose([Validators.maxLength(30), Validators.required])]
    });
    this.nav = nav;


  }

  login() {
    let credentials = { username: this.username, password: this.password }
    this.auth.login(credentials).then(
      (success) => {
        this.events.publish('logged-in');
        this.nav.setRoot(HomePage);
      },
      (err) => {
        console.log(err)
        let alert = this.alertCtrl.create({
          title: 'Oops!',
          subTitle: err.json()['message'],
          buttons: ['OK']
        });
        alert.present();
      });
  }

  goToSignUp() {
    this.nav.push(SignUpPage);
  }

}