import { Component, ViewChild, NgModule } from '@angular/core';
import { Platform, Nav, IonicApp, IonicModule } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { HomePage } from '../pages/home/home';
import { AboutPage } from '../pages/about/about';
import { SuccessPage } from '../pages/success/success';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { LoginPage } from '../pages/login/login';
import { AddPage } from '../pages/add/add';
import { TodayPage } from '../pages/today/today';
import { ProfilePage } from '../pages/profile/profile';
import { GoogleMapsAddPage } from '../pages/google-maps-add/google-maps-add';
import { EventsService } from '../providers/events-service';
import { MapsService } from '../providers/maps-service';
import { AuthService } from '../providers/auth-service';
import { BrowserModule } from '@angular/platform-browser';
import { AuthHttp, JwtHelper } from 'angular2-jwt';
import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})


export class MyApp {
  private rootPage = HomePage;
  private signUpPage = SignUpPage;
  private aboutPage = AboutPage;
  private homePage = HomePage;
  private loginpage = LoginPage;
  private profilePage = ProfilePage;
  private todayPage = TodayPage;

  public pages: Array<{ title: string, component: any }>;
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform, public auth: AuthService, public events: Events) {
    this.auth = auth;
    this.events.subscribe('logged-in', () => {
      this.pages = [
        { title: 'Home', component: HomePage }, { title: 'Today\'s Events', component: TodayPage }];
      if (this.auth.loggedIn()) {
        this.pages.push(
          { title: 'Profile', component: ProfilePage },
          { title: 'Logout', component: undefined });
      }
      else {
        this.pages.push({ title: 'Log-In/Sign Up', component: LoginPage })
      }
      this.pages.push({ title: 'About', component: AboutPage });

    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      StatusBar.styleDefault();
      Splashscreen.hide();
      this.pages = [
        { title: 'Home', component: HomePage }, { title: 'Today\'s Events ', component: TodayPage }];
      if (this.auth.loggedIn()) {
        this.pages.push(
          { title: 'Profile', component: ProfilePage },
          { title: 'Logout', component: undefined });
      }
      else {
        this.pages.push({ title: 'Log-In/Sign Up', component: LoginPage })
      }
      this.pages.push({ title: 'About', component: AboutPage });

    });
  }


  openPage(page) {
    if (page.title == "Logout") {
      console.log("logged out")
      this.auth.logout();
      this.pages = [{ title: 'Home', component: HomePage }, { title: 'Today\'s Events', component: TodayPage }, { title: 'Log-In/Sign Up', component: LoginPage }, { title: 'About', component: AboutPage }];
      this.nav.setRoot(HomePage);
    }
    else {
      this.nav.setRoot(page.component);
    }


  }


}
