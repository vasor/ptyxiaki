import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AddButtonPage } from '../pages/add-button/add-button';
import { AddPage } from '../pages/add/add';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { LoginPage } from '../pages/login/login';
import { AboutPage } from '../pages/about/about';
import { TodayPage } from '../pages/today/today';
import { EventModal } from '../pages/event-modal/event-modal';
import { ProfilePage } from '../pages/profile/profile';
import { EditEventPage } from '../pages/edit-event/edit-event';
import { SuccessPage } from '../pages/success/success';
import { GoogleMapsAddPage } from '../pages/google-maps-add/google-maps-add';
import { EventsService } from '../providers/events-service';
import { User } from '../providers/user';
import { ConnectivityService } from '../providers/connectivity-service';
import { MapsService } from '../providers/maps-service';
import { Geolocation } from 'ionic-native';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AuthService } from '../providers/auth-service';
import { Network } from '@ionic-native/network';
import { Diagnostic } from '@ionic-native/diagnostic';
import { AndroidPermissions } from '@ionic-native/android-permissions';

let storage = new Storage();

export function getAuthHttp(http) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true,
    globalHeaders: [{ 'Accept': 'application/json' }],
    tokenGetter: (() => storage.get('access_token')),
  }), http);
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignUpPage,
    AboutPage,
    AddButtonPage,
    AddPage,
    EditEventPage,
    ProfilePage,
    GoogleMapsAddPage,
    LoginPage,
    SuccessPage,
    TodayPage,
    EventModal
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignUpPage,
    AboutPage,
    AddButtonPage,
    EditEventPage,
    AddPage,
    ProfilePage,
    GoogleMapsAddPage,
    LoginPage,
    SuccessPage,
    TodayPage,
    EventModal
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AndroidPermissions, 
    Diagnostic, 
    Network, 
    EventsService, 
    MapsService, 
    AuthService, 
    ConnectivityService, 
    Geolocation, 
    User, 
    {provide: AuthHttp, useFactory: getAuthHttp, deps: [Http]
  }]
})
export class AppModule { }
