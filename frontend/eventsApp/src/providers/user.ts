import { Injectable, ViewChild } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { App, AlertController, LoadingController } from 'ionic-angular';

@Injectable()
export class User {
  // @ViewChild(NavController) nav;
  public nav: any;
  public token: any;
  public loading: any;
  public result: any;
  constructor(public http: Http, private app: App, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    console.log('Hello User Provider');
    this.nav = this.app.getActiveNav();
  }


  signup(firstname, lastname, username, email, password) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let body = {
      firstname: firstname,
      lastname: lastname,
      username: username,
      email: email,
      password: password
    };

    return this.http.post('https://young-cliffs-17923.herokuapp.com/signup', JSON.stringify(body), { headers: headers })
      .map(res => res.json())
  }


  getInfo(user_id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let body = {
      user_id: user_id
    };

    return this.http.post('https://young-cliffs-17923.herokuapp.com/info', JSON.stringify(body), { headers: headers })
      .map(res => res.json())
  }
}
