import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Geolocation } from 'ionic-native';

@Injectable()
export class MapsService {

  constructor(public http: Http, public geolocation: Geolocation) {
    console.log('Hello MapsService Provider');
  }

  getCurrentPosition() {
    return Geolocation.getCurrentPosition();
  }

  watchPosition() {
    return Geolocation.watchPosition();
  }
}
