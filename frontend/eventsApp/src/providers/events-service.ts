import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { AuthService } from './auth-service';


@Injectable()
export class EventsService {

  constructor(public http: Http, public auth: AuthService) {
    console.log('Hello EventsService Provider');
  }

  getList() {
    return this.http.get("https://young-cliffs-17923.herokuapp.com/list")
      .map(res => res.json());
  }
  getListAll() {
    return this.http.get("https://young-cliffs-17923.herokuapp.com/all")
      .map(res => res.json());
  }

  addEvent(ptitle, pdescription, paddress, pprice, pdate, pstarting_time, pexpexted_duration, pposition) {
    let user_id = this.auth.user;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let body = {
      title: ptitle,
      description: pdescription,
      address: paddress,
      price: pprice,
      date: pdate,
      starting_time: pstarting_time,
      expected_duration: pexpexted_duration,
      position: pposition,
      user_id: user_id
    };

    return new Promise((resolve, reject) => {
      this.http.post('https://young-cliffs-17923.herokuapp.com/insert', JSON.stringify(body), { headers: headers })
        .map(res => res.json()).subscribe(data => {
          console.log(data);
          resolve(data)
        }, error => {
          console.log(error)
          reject(error)
        })
    })
  }

  deleteEvent(event_id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let body = {
      event_id: event_id
    };

    return this.http.post('https://young-cliffs-17923.herokuapp.com/delete_event', JSON.stringify(body), { headers: headers })
      .map(res => res.json())
  }

  editEvent(pevent_id, ptitle, pdescription, paddress, pprice, pdate, pstarting_time, pexpexted_duration, pposition) {
    let user_id = this.auth.user;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let body = {
      event_id: pevent_id,
      title: ptitle,
      description: pdescription,
      address: paddress,
      price: pprice,
      date: pdate,
      starting_time: pstarting_time,
      expected_duration: pexpexted_duration,
      position: pposition,
      user_id: user_id
    };

    return new Promise((resolve, reject) => {
      this.http.post('https://young-cliffs-17923.herokuapp.com/update_event', JSON.stringify(body), { headers: headers })
        .map(res => res.json()).subscribe(data => {
          console.log(data);
          resolve(data)
        }, error => {
          console.log(error)
          reject(error)
        })
    }
    )
  }
}
