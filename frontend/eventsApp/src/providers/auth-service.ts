import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { MyApp } from '../app/app.component';
import 'rxjs/add/operator/map';
@Injectable()
export class AuthService {
    LOGIN_URL: string = "https://young-cliffs-17923.herokuapp.com/login";
    SIGNUP_URL: string = "https://young-cliffs-17923.herokuapp.com/signup";
    contentHeader: Headers = new Headers({ "Content-Type": "application/json" });
    jwtHelper: JwtHelper = new JwtHelper();
    user: string;
    error: string;
    constructor(private http: Http) {
        let token = localStorage.getItem('token');
        if (token) {
            this.user = this.jwtHelper.decodeToken(token)['sub'];
        }
    }
    public authenticated() {
        return tokenNotExpired();
    }
    login(credentials) {
        return new Promise((resolve, reject) => {
            this.http.post(
		this.LOGIN_URL, JSON.stringify(credentials), { headers: this.contentHeader }
	    )
                .map(res => res.json())
                .subscribe(
                data => {
                    this.authSuccess(data.token);
                    resolve(data)
                },
                err => {
                    this.error = err;
                    reject(err)
                }
                );
        });
    }
    signup(credentials) {
        return new Promise((resolve, reject) => {
            this.http.post(this.SIGNUP_URL, JSON.stringify(credentials), { headers: this.contentHeader })
                .map(res => res.json())
                .subscribe(
                (data) => {
                    this.authSuccess(data.token);
                    resolve(data)
                },
                err => {
                    this.error = err;
                    reject(err)
                }
                );
        });
    }
    logout() {
        localStorage.removeItem('token');
        this.user = null;
    }
    authSuccess(token) {
        this.error = null;
        localStorage.setItem('token', token);
        this.user = this.jwtHelper.decodeToken(token)['sub'];
        console.log(this.user);
    }


    loggedIn() {
        return tokenNotExpired();
    }
}
